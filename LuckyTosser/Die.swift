//
//  Die.swift
//  LuckyTosser
//
//  Created by JeremyRaven on 3/02/2016.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import SceneKit


class Die: SCNNode {

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Designated Initialiser
    init(num: Int, importedCube: SCNNode, scale: SCNVector3) {
        
        let die: SCNNode = importedCube.copy() as! SCNNode
        
        if importedCube.name == "StandardDice" {
            die.scale = SCNVector3(75, 75, 75)
        } else if importedCube.name == "PyramidDice" {
            die.scale = SCNVector3(140, 140, 140)
        } else if importedCube.name == "DodecagonDice" {
            die.scale = SCNVector3(60, 60, 60)
        } else if importedCube.name == "IcosahedronDice" {
            die.scale = SCNVector3(60, 60, 60)
        }  else if importedCube.name == "OctogonDice" {
            die.scale = SCNVector3(70, 70, 70)
        }
        
        //die.scale = scale
        
        super.init()
        
        self.name = "dice" + String(num)
        self.addChildNode(die)
        let collisionBox = SCNBox(width: 5.0, height: 5.0, length: 5.0, chamferRadius: 0)
        self.physicsBody?.physicsShape = SCNPhysicsShape(geometry: collisionBox, options: nil)
        self.physicsBody = SCNPhysicsBody.dynamic()
        self.physicsBody?.mass = 0.1
        self.physicsBody?.restitution = 0.8
        self.physicsBody?.damping = 0.5
        self.physicsBody?.allowsResting = true

        self.eulerAngles = SCNVector3Make( GLKMathDegreesToRadians(randomFloat(min: 0, max: 360)),
                                           GLKMathDegreesToRadians(randomFloat(min: 0, max: 360)),
                                           GLKMathDegreesToRadians(randomFloat(min: 0, max: 360)))
        self.position = SCNVector3Make(0, 130, 0)
    }
    
    func randomFloat(min: Float, max:Float) -> Float {
        return min + Float(arc4random_uniform(UInt32(max - min + 1)))
    }
    
}
