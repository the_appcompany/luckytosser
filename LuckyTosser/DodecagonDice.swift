//
//  DodecagonDice.swift
//  LuckyTosser
//
//  Created by Steve on 10/07/18.
//  Copyright © 2018 TheAppCompany. All rights reserved.
//

import Foundation

struct DodecagonDice {
    static let nibName              = "Dice1ViewController"
    static let bundle               = Bundle.main
    static let boxImageFile         = "art.scnassets/DiceWallpaper.png"
    static let diceScene            = "art.scnassets/DodecagonDice.dae"
    static let childNodeName        = "DodecagonDice"
    static let rollTossSoundFile    = "art.scnassets/RollDice"
}
