//
//  LuckyTosserViewController.swift
//  LuckyTosser
//
//  Created by Steve on 25/02/16.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

/* Do not instantiate objects of this class, rather instantiate objects
 * of its subclasses
 */

import AVFoundation
import CoreMotion
import SceneKit
import UIKit

class LuckyTosserViewController: UIViewController, SCNSceneRendererDelegate
{
    var animator: UIDynamicAnimator!
    var attachment: UIAttachmentBehavior!
    var collisionBox: CollisionBox!
    var constraint: ConstraintObject!
    var itemBehavior: UIDynamicItemBehavior!
    let panRec = UIPanGestureRecognizer()
    var push: UIPushBehavior!
    var scnView: SCNView!
    var snap: UISnapBehavior!
    var theScene: SCNScene!

    // Properties for init method
    let boxImageFile: String
    let childNodeName: String
    let rollTossSoundFile: String
    let scene: String

    // Variables and constants abstracted from CoinViewController
    // and DiceViewController
    var rollTossSoundAudioPlayer: AVAudioPlayer!
    let Gravity: Float = 0
    var _i: Int = 0
    var importedNode: SCNNode!
    var importedScene: SCNScene!
    let impulseForce: CGFloat = 1.5
    var motionManager: CMMotionManager!
    var posX: CGFloat = 0.0
    var posY: CGFloat = 0.0
    var rect: CGRect!
    var scale: SCNVector3!
    var _size: CGSize!
    var rollTossSound: URL?
    
    
    
    // OK
    // MARK: Designated initialiser
    
    init(
        nibName: String?,
        bundle: Bundle,
        boxImageFile: String,           // "art.scnassets/dice1_1420x2205.png"
        scene: String,                  // "art.scnassets/diceFourSides3.dae"
        childNodeName: String,          // "StandardDice"
        rollTossSoundFile: String) {
        
            self.boxImageFile = boxImageFile
            self.scene = scene
            self.childNodeName = childNodeName
            self.rollTossSoundFile = rollTossSoundFile
            super.init(nibName: nibName, bundle: bundle)
    }

    // OK
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // OK
    func addCameraNode(constraintboxNode: SCNNode) {
        
        let camera = SCNCamera()
        camera.yFov = 40
        let cameraNode = SCNNode()
        cameraNode.camera = camera
        cameraNode.position = SCNVector3(0, 800, 0)
        cameraNode.camera?.zNear = 0.1
        cameraNode.camera?.zFar = 1200

        // Constrain camera to constraint
        let constraint = SCNLookAtConstraint(target: constraintboxNode)
        cameraNode.constraints = [constraint]

        theScene.rootNode.addChildNode(cameraNode)
        
        scnView.pointOfView = cameraNode
    }
    
    // OK
    func addLightSourceNode(constraintboxNode: SCNNode) {
        
        // Spotlight1
        let light = SCNLight()
        let lightNode = SCNNode()
        light.type = SCNLight.LightType.spot
        light.spotInnerAngle = 60.0
        light.spotOuterAngle = 100.0
        light.castsShadow = true
        light.shadowColor = UIColor.black
        light.zFar = 1000
        lightNode.light = light
        lightNode.position = SCNVector3(30, 250, -60)
        
        // Ambient light
        let ambientLight = SCNLight()
        let ambientLightNode = SCNNode()
        ambientLight.type = SCNLight.LightType.ambient
        ambientLight.color = UIColor.darkGray
        ambientLightNode.light = ambientLight
        ambientLightNode.position = SCNVector3(10, 10, 10)
        
        // Constrain light to boxConstraint
        let constraint = SCNLookAtConstraint(target: constraintboxNode)
        lightNode.constraints = [constraint]
        
        // Add lights to scene
        theScene.rootNode.addChildNode(lightNode)
        theScene.rootNode.addChildNode(ambientLightNode)
    }
    
    // OK
    func makeBoxWithImageNamed(boxImageFile: String) {
        
        // Using UIImage? to explicitly show image may be nil
        let image: UIImage? = UIImage(named: boxImageFile)
        
        if let _image = image {
            collisionBox = CollisionBox(scnView: scnView, image: _image)
            theScene.rootNode.addChildNode(collisionBox)
        }
    }
    
    // OK
    func makeConstraintObject() {
        constraint = ConstraintObject()
        theScene.rootNode.addChildNode(constraint)
    }
    
    // *** Not sure if a float or an int is needed here ***
    func randomFloat(min: Float, max:Float) -> Float {
        return min + Float(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    // OK
    func loadMeshObjects() {
        
        importedScene = SCNScene(named: scene)
        importedNode = importedScene.rootNode.childNode(withName: childNodeName, recursively: true)
    }
    
    // OK
    func loadSoundFX() {
        
        rollTossSound = Bundle.main.url(forResource:rollTossSoundFile, withExtension: "mp3")
    }

    // OK
    func playRollTossSoundFX() {
        
        if let soundFile = rollTossSound {
            do {
                try rollTossSoundAudioPlayer = AVAudioPlayer(contentsOf: soundFile)
                rollTossSoundAudioPlayer.play()
                
            } catch {
                print("Failed to play rollTossSound: \(soundFile)")
            } // End of do-catch
            
        } else {
            print("URL for rollTossSound is nil.")
        } // End of if-else
    }

//    // OK - Jeremy should probably test to ensure it works as expected.
//    func dragSliderView(gesture:UIPanGestureRecognizer) {
//
//        let translation: CGPoint
//        self.view.bringSubview(toFront: gesture.view!)
//        translation = gesture.translation(in: self.view)
//
//        switch (gesture.state){
//        case UIGestureRecognizerState.began:
//
//            break
//
//        case UIGestureRecognizerState.changed:
//
//            gesture.view!.center = CGPoint(x: gesture.view!.center.x + translation.x, y: gesture.view!.center.y)
//            print("SliderView center is \(gesture.view!.center.x)")
//
//        case UIGestureRecognizerState.ended:
//
//            var finalX: CGFloat = translation.x + (2.0 * (gesture.velocity(in: self.view).x))
//            print("FinalX is \(finalX)")
//            //var finalY: CGFloat = translation.y + (0.0 * (gesture.velocityInView(self.view).y))
//
//            if finalX > 289 {
//                finalX = 289
//            }
//            if finalX < 88 {
//                finalX = 88
//            }
//
//            UIView.animate(withDuration:0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options:  .curveEaseIn, animations: {gesture.view!.center.x = finalX}, completion: nil)
//
//        default:
//            break
//        }
//
//        gesture.setTranslation(CGPoint.zero, in: self.view)
//    }
    
//    // MARK: UIViewController
//
//    // OK, but not sure it is required
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Release any cached data, images, etc that aren't in use.
//    }
//
//    //OK
//    /* From the Apple documentation...
//     * If you change the return value for this method, call the
//     * setNeedsStatusBarAppearanceUpdate() method. To specify that a child view controller
//     * should control preferred status bar hidden/unhidden state, implement the
//     * childViewControllerForStatusBarHidden method.
//     */
    override var prefersStatusBarHidden: Bool {
        get {
            return true;
        }
    }
//
//    // OK
//    /* From the Apple documentation...
//     * This method returns true by default.
//     */
//    override var shouldAutorotate: Bool {
//        get {
//            return true;
//        }
//    }
//
//    // OK
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        get {
//            if UIDevice.current.userInterfaceIdiom == .phone {
//                return UIInterfaceOrientationMask.allButUpsideDown
//            } else {
//                return UIInterfaceOrientationMask.all
//            }
//        }
//    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        view.frame = UIScreen.main.bounds
        scnView = view as! SCNView                                     // Downcast view to a SCNView
        
        posX = scnView.bounds.width
        posY = scnView.bounds.height
        rect = CGRect(x: 0, y: posY, width: posX, height: 100)
        // scnView.allowsCameraControl = true
        // scnView.showsStatistics = true
        // _size = scnView.bounds.size
        
        theScene = SCNScene()                                               // Create instance of SCNScene
        //theScene.physicsWorld.gravity = SCNVector3(0, Gravity, 0)
        
        scnView.scene = theScene                                            // Add SCNScene to current view
        scnView.delegate = self
        
//        panRec.addTarget(self, action: "dragSliderView:")                 // Setup pan gesture
//        panRec.addTarget(self, action: #selector(LuckyTosserViewController.dragSliderView(gesture:)))
        
        // Setup
        motionManager = CMMotionManager()                                   // Start the Accelerometer
        motionManager.startAccelerometerUpdates()
        
        animator = UIDynamicAnimator(referenceView: view)                   // Init UIDynamicAnimator
        
        //scnView.debugOptions = .showPhysicsShapes                         // Show physics collision bodies
        
        // Create ButtonsView - add/delete and home buttons
        let buttonsView = ButtonsView(frame: scnView.bounds)
        view.addSubview(buttonsView)

        // Call functions
        loadMeshObjects()                                                    // Load mesh objects
        loadSoundFX()                                                        // Load SoundFX
        makeConstraintObject()                                               // Make constraint
        addLightSourceNode(constraintboxNode: constraint)                    // Add light
        addCameraNode(constraintboxNode: constraint)                         // Add Camera
        makeBoxWithImageNamed(boxImageFile: boxImageFile)
    }
    
    
    // MARK: Segue Method
    
    @objc func segueToHomeMenu() {
        
        print("Inside func segueToHOmeMenu of LuckyTosserViewController")
        
        if let controller = self.presentingViewController {
            /* From the documentation...
             *
             * If you present several view controllers in succession, thus building a stack of
             * presented view controllers, calling this method (dismissViewControllerAnimated)
             * on a view controller lower in the stack dismisses its immediate child view
             * controller and all view controllers above that child on the stack.
             */
            controller.dismiss(animated: true, completion: nil)
            
        } else {
            print("self.presentingViewController is nil, could not dismiss")
        }
    }

    
}
