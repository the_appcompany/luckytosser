//
//  HomeMenuViewController.swift
//  LuckyTosser
//
//  Created by Steve on 26/06/18.
//  Copyright © 2018 TheAppCompany. All rights reserved.
//

import UIKit

class HomeMenuViewController: UIViewController {

    let screenSize = UIScreen.main.bounds
    let buttonSize = Int(UIScreen.main.bounds.width/4)
    
    // Array of tuples
    var positionsArray: [(Int, Int)] = []
    
    let imagesArray = ["MenuItem_Cube1_sm.jpg",
                       "MenuItem_Cube2_sm.jpg",
                       "MenuItem_Cube3_sm.jpg",
                       "MenuItem_Cube4_sm.jpg",
                       "MenuItem_Cube5_sm.jpg",
                       "MenuItem_Cube6_sm.jpg"]
    
    let seguesArray = [#selector(HomeMenuViewController.segueToIcosahedronDice),
                       #selector(HomeMenuViewController.segueToStandardDice),
                       #selector(HomeMenuViewController.segueToOctogonDice),
                       #selector(HomeMenuViewController.segueToPyramidDice),
                       #selector(HomeMenuViewController.segueToDodecagonDice),
                       #selector(HomeMenuViewController.segueToRedDragonCoin)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        // RGB values are between 0-1 -> Divide by 255 if necessary
        self.view.backgroundColor = UIColor(red: 0.0, green: 0.25, blue: 0.133, alpha: 1.0)
        
        // Initialise button arrays
        positionsArray = initButtonPositionsArray()
        createButtons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var prefersStatusBarHidden: Bool {
        get {
            return true;
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

    func initButtonPositionsArray()->[(Int, Int)] {
        
        // Calculate button size and screen position from main.bounds
        let buttonHalfSize = buttonSize/2
        let halfWidth = Int(screenSize.width/2)
        let halfHeight = Int(screenSize.height/2)
        let buttonGap: Int = 3
        
        // [(x, y), ...]
        return [(halfWidth - buttonSize - buttonHalfSize - buttonGap,   halfHeight - buttonSize - 10),
                (halfWidth - buttonHalfSize,                            halfHeight - buttonSize - 10),
                (halfWidth + buttonHalfSize + buttonGap,                halfHeight - buttonSize - 10),
                (halfWidth - buttonSize - buttonHalfSize - buttonGap,   halfHeight + buttonGap - 10),
                (halfWidth - buttonHalfSize,                            halfHeight + buttonGap - 10),
                (halfWidth + buttonHalfSize + buttonGap,                halfHeight + buttonGap - 10)]
    }
    
    func createButtons() {
        
        //print("Inside createButtons in HomeMenuViewController")
        for i in 0..<imagesArray.count {
            if let image = UIImage(named: imagesArray[i]) {
                let (x, y) = positionsArray[i]
                let button: UIButton = createButton(image: image,
                                                    x: x,
                                                    y: y,
                                                    width: buttonSize,
                                                    height: buttonSize,
                                                    opacity: 1.0,
                                                    action: seguesArray[i])
                view.addSubview(button)
                
            } else {
                print("Image is nil - imagesArray[\(i)]: \(imagesArray[i]).")
            }
        } // for block
    }
    
    
    // MARK: Segue Methods
    
    @objc func menuOption1() {
        
        print("Inside func menuOption1 of HomeMenuViewController")
        segueToIcosahedronDice()
    }
    
    @objc func segueToIcosahedronDice() {
        
        print("Inside func segueToIcosahedronDice of HomeMenuViewController")
        
        let controller = DiceViewController(nibName: IcosahedronDice.nibName,
                                            bundle: IcosahedronDice.bundle,
                                            boxImageFile: IcosahedronDice.boxImageFile,
                                            diceScene: IcosahedronDice.diceScene,
                                            childNodeName: IcosahedronDice.childNodeName,
                                            rollTossSoundFile: IcosahedronDice.rollTossSoundFile)
        
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func segueToStandardDice() {
        
        print("Inside func segueToStandardDice of HomeMenuViewController")
        
        let controller = DiceViewController(nibName: StandardDice.nibName,
                                            bundle: StandardDice.bundle,
                                            boxImageFile: StandardDice.boxImageFile,
                                            diceScene: StandardDice.diceScene,
                                            childNodeName: StandardDice.childNodeName,
                                            rollTossSoundFile: StandardDice.rollTossSoundFile)

        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func segueToOctogonDice() {
        
        print("Inside func segueToOctogonDice of HomeMenuViewController")
        
        let controller = DiceViewController(nibName: OctogonDice.nibName,
                                            bundle: OctogonDice.bundle,
                                            boxImageFile: OctogonDice.boxImageFile,
                                            diceScene: OctogonDice.diceScene,
                                            childNodeName: OctogonDice.childNodeName,
                                            rollTossSoundFile: OctogonDice.rollTossSoundFile)
        
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func segueToPyramidDice() {
        
        print("Inside func segueToPyramidDice of HomeMenuViewController")
        
        let controller = DiceViewController(nibName: PyramidDice.nibName,
                                            bundle: PyramidDice.bundle,
                                            boxImageFile: PyramidDice.boxImageFile,
                                            diceScene: PyramidDice.diceScene,
                                            childNodeName: PyramidDice.childNodeName,
                                            rollTossSoundFile: PyramidDice.rollTossSoundFile)
        
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func segueToDodecagonDice() {
        
        print("Inside func segueToDodecagonDice of HomeMenuViewController")
        
        let controller = DiceViewController(nibName: DodecagonDice.nibName,
                                            bundle: DodecagonDice.bundle,
                                            boxImageFile: DodecagonDice.boxImageFile,
                                            diceScene: DodecagonDice.diceScene,
                                            childNodeName: DodecagonDice.childNodeName,
                                            rollTossSoundFile: DodecagonDice.rollTossSoundFile)
        
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func segueToRedDragonCoin() {
        
        print("Inside func segueToRedDragonCoin of HomeViewController")
        
        let controller = CoinViewController(nibName: RedDragonCoin.nibName,
                                            bundle: RedDragonCoin.bundle,
                                            boxImageFile: RedDragonCoin.boxImageFile,
                                            coinScene: RedDragonCoin.coinScene,
                                            childNodeName: RedDragonCoin.childNodeName,
                                            rollTossSoundFile: RedDragonCoin.rollTossSoundFile)
        
        self.present(controller, animated: true, completion: nil)
    }
}
