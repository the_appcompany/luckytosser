//
//  Utility.swift
//  LuckyTosser
//
//  Created by Steve on 26/06/18.
//  Copyright © 2018 TheAppCompany. All rights reserved.
//

import Foundation
import UIKit

func createButton(image: UIImage,
                  x: Int,
                  y: Int,
                  width: Int,
                  height: Int,
                  opacity: Float,
                  action: Selector) -> UIButton {
    
    let button = UIButton()
    button.layer.opacity = opacity
    button.layer.shadowColor = UIColor.black.cgColor
    button.layer.shadowOffset = CGSize(width: -3, height: 3)
    button.layer.shadowRadius = 1
    button.layer.shadowOpacity = 0.3
    button.setBackgroundImage(image, for: .normal)
    button.frame = CGRect(x:x, y:y, width:width, height:height)
    
    print("action: \(action)")

    button.addTarget(nil, action: action, for: .touchUpInside)
    return button
}
