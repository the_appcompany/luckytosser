//
//  CollisionBox.swift
//  LuckyTosser
//
//  Created by JeremyRaven on 11/02/2016.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import SceneKit


class CollisionBox: SCNNode {
    
    var _size: CGSize!
    var _image: UIImage!

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Designated Initialiser
    init(scnView: SCNView, image: UIImage) {

        _image = image
        _size = scnView.bounds.size
        let plane = SCNPlane(width: _size.width, height: _size.height)
        
        super.init()
        
        self.wallsForBox(plane: plane)
    }
    
    func wallsForBox(plane: SCNPlane) {
        // Start making the walls
        
        func physicsWall(width: CGFloat, height: CGFloat) -> SCNNode {
            // Basic plane initiated
            
            let plane = SCNPlane(width: width, height: height)
            let material1 = SCNMaterial()
            material1.diffuse.contents =  _image
            plane.materials = [material1]
            
            let node = SCNNode(geometry: plane)
            node.physicsBody?.physicsShape = SCNPhysicsShape(geometry: plane, options: nil)
            node.physicsBody = .static()
            node.physicsBody?.restitution = 0.5
            node.physicsBody?.friction = 0.5
            return node
        }
        
        func physicsBox(width: CGFloat, height: CGFloat) -> SCNNode {
            
            let box = SCNBox(width: width, height: height, length: 5, chamferRadius: 0)
            box.heightSegmentCount = 5
            box.widthSegmentCount = 5
            let material1 = SCNMaterial()
            material1.diffuse.contents = _image
            box.materials = [material1]
            
            let node = SCNNode(geometry: box)
            node.physicsBody?.physicsShape = SCNPhysicsShape(geometry: box, options: nil)
            node.physicsBody = .static()
            node.physicsBody?.restitution = 0.3
            node.physicsBody?.friction = 3.5
            return node
        }
        
        // Call physicsWall to create the basic plane
        let leftWall = physicsWall(width: plane.height, height: plane.width)
        leftWall.position.x = (Float(-plane.width) / 2.5)
        leftWall.position.y = Float(plane.width) / 2
        leftWall.eulerAngles = SCNVector3Make( 0, GLKMathDegreesToRadians(-90), 0)
        leftWall.opacity = 0
        self.addChildNode(leftWall)
        
        let rightWall = physicsWall(width: plane.height, height: plane.width)
        rightWall.position.x = (Float(plane.width) / 2.5)
        rightWall.position.y = Float(plane.width) / 2
        rightWall.eulerAngles = SCNVector3Make( 0, GLKMathDegreesToRadians(90), 0)
        rightWall.opacity = 0
        self.addChildNode(rightWall)
        
        let frontWall = physicsWall(width: plane.width, height: plane.width)
        frontWall.position.z = Float(plane.height) / 3
        frontWall.position.y = Float(plane.width) / 2
        frontWall.opacity = 0
        self.addChildNode(frontWall)
        
        let backWall = physicsWall(width: plane.width, height: plane.width)
        backWall.position.z = Float(-plane.height) / 2.2
        backWall.position.y = Float(plane.width) / 2
        backWall.opacity = 0
        self.addChildNode(backWall)
        
        let bottomWall1 = physicsBox(width: plane.width, height: plane.height)
        bottomWall1.position = SCNVector3(0, 0, -55) // -55   Float(-plane.height) / 2.0
        bottomWall1.eulerAngles = SCNVector3Make(GLKMathDegreesToRadians(-90), 0, 0)
        bottomWall1.name = "groundPlane1"
        self.addChildNode(bottomWall1)
        
        let topWall = physicsWall(width: plane.width, height: plane.height)
        topWall.position.y = Float(plane.height)/2
        topWall.eulerAngles = SCNVector3Make(GLKMathDegreesToRadians(90), 0, 0)
        topWall.opacity = 0
        self.addChildNode(topWall)
        
        self.position = SCNVector3Make(0, 0, 0)
    }

}

