//
//  PyramidDice.swift
//  LuckyTosser
//
//  Created by Steve on 10/07/18.
//  Copyright © 2018 TheAppCompany. All rights reserved.
//

import Foundation

struct PyramidDice {
    static let nibName              = "Dice1ViewController"
    static let bundle               = Bundle.main
    static let boxImageFile         = "art.scnassets/DiceWallpaper.png"
    static let diceScene            = "art.scnassets/PyramidDice.dae"
    static let childNodeName        = "PyramidDice"
    static let rollTossSoundFile    = "art.scnassets/RollDice"
}
