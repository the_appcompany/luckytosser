//
//  ButtonsView.swift
//  LuckyTosser
//
//  Created by Steve on 26/06/18.
//  Copyright © 2018 TheAppCompany. All rights reserved.
//

import UIKit

class ButtonsView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    let buttonSize: Int
    let buttonGap: Int = 10
    let screenSize: CGRect
    
    // Array of tuples
    var positionsArray: [(Int, Int)] = []
    
    let imagesArray = ["addBut.png",
                       "homeBut.png",
                       "deleteBut.png"]
    
    let seguesArray = [#selector(LuckyTosserProtocol.addObject(sender:)),
                       #selector(LuckyTosserViewController.segueToHomeMenu),
                       #selector(LuckyTosserProtocol.deleteObject(sender:))]
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        
        screenSize = frame
        buttonSize = Int(frame.width/5)
        
        let customFrame = CGRect(x: 0,
                                 y: Int(frame.height) - buttonGap - buttonSize - buttonGap,
                                 width: Int(frame.width),
                                 height: buttonGap + buttonSize + buttonGap)
        
        super.init(frame: customFrame)
        
        // RGB values are between 0-1 -> Divide by 255 if necessary
        backgroundColor = UIColor(red: 0.0, green: 0.25, blue: 0.133, alpha: 0.5)
        
        positionsArray = initButtonPositionsArray()
        createButtons()
    }
    
    func initButtonPositionsArray()->[(Int, Int)] {
        
        // Calculate button size and screen position from frame passed to initialiser (i.e. screenSize)
        let buttonHalfSize = buttonSize/2
        let halfWidth = Int(screenSize.width/2)
        
        // [(x, y), ...]
        return [(buttonGap,                                         buttonGap),
                (halfWidth - buttonHalfSize,                        buttonGap),
                (Int(screenSize.width) - buttonSize - buttonGap,    buttonGap)]
    }
    
    func createButtons() {
        
        for i in 0..<imagesArray.count {
            
            if let image = UIImage(named: imagesArray[i]) {
                let (x, y) = positionsArray[i]
                let button: UIButton = createButton(image: image,
                                                    x: x,
                                                    y: y,
                                                    width: buttonSize,
                                                    height: buttonSize,
                                                    opacity: 0.3,
                                                    action: seguesArray[i])
                addSubview(button)
            } // if block
        } // for block
    }
    
}

