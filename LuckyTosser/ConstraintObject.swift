//
//  ConstraintObject.swift
//  LuckyTosser
//
//  Created by JeremyRaven on 11/02/2016.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import SceneKit

class ConstraintObject: SCNNode {
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Designated Initialiser
    override init() {
        let node: SCNNode

        super.init()
        
        let constraintbox = SCNBox()
        node = SCNNode(geometry: constraintbox)
        self.addChildNode(node)
        self.opacity = 0
        self.position = SCNVector3Make(0, 0, -30)
        self.scale = SCNVector3Make(40, 40, 40)
    }
}
