//
//  CoinViewController.swift
//  LuckyTosser
//
//  Created by Steve on 4/04/16.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import AVFoundation
import CoreMotion
import QuartzCore
import SceneKit
import UIKit

class CoinViewController: LuckyTosserViewController, LuckyTosserProtocol {
    
    var coin: Coin?
    
    // MARK: Designated initialiser
    
    init(
        nibName: String?,
        bundle: Bundle,
        boxImageFile: String,
        coinScene: String,
        childNodeName: String,
        rollTossSoundFile: String) {
        
            super.init(
                nibName: nibName,
                bundle: bundle,
                boxImageFile: boxImageFile,
                scene: coinScene,
                childNodeName: childNodeName,
                rollTossSoundFile: rollTossSoundFile)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

//    func spinCoinBtn() {
//
//        let spinCoinBtn = UIButton(type: UIButtonType.custom) as UIButton
//        spinCoinBtn.frame = CGRect(x: posX / 2 - 40, y: posY / 3 + 250, width: 100, height: 100)
//        spinCoinBtn.layer.cornerRadius = 50;
//        spinCoinBtn.clipsToBounds = true;
//        spinCoinBtn.backgroundColor = UIColor.gray
//        spinCoinBtn.setTitle("tap tap", for: .normal)
//
//        //        spinCoinBtn.addTarget(nil, action:Selector("applyForce:"), for:.touchUpInside)
//        spinCoinBtn.addTarget(nil, action: #selector(applyForce(sender:)), for: .touchUpInside)
//
//        scnView.addSubview(spinCoinBtn)
//    }
    

    
    // MARK: LuckyTosserProtocol
    
    func addObject(sender: UIButton?) {

//        print("Inside func addObject(sender:) of CoinViewController")
        if let _coin = coin {
            _coin.removeFromParentNode()
        }

        coin = Coin(importedCoin: importedNode, scale: scale)
        theScene.rootNode.addChildNode(coin!)
    }
    
    func applyForce()
    {
        if let _ = motionManager.accelerometerData
        {
            playRollTossSoundFX()
            coin!.physicsBody?.applyForce(
                SCNVector3Make(0, randomFloat(min: 800, max: 1000), 0),
                at: SCNVector3Make(0, 0, 100),
                asImpulse: true)
            
        }
        if let accelerometerData = motionManager.accelerometerData          // Make objects stick to table
        {
            let accelX = (abs(accelerometerData.acceleration.x))
            if (accelX < 0.3)
            {
                theScene.physicsWorld.gravity = SCNVector3Make(0, -5000, 0)
            }
        }
    }
    
    func deleteObject(sender:UIButton) {
        
//        print("Inside func deleteObject(sender:) of CoinViewController")
        if let _coin = coin {
            _coin.removeFromParentNode()
        }
    }
    
    // OK
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        update()
    }

    // OK
    func update() {
        // *** This causes the coin to toss upward, out of view. Ironically seems to behave OK
        // *** when not called???
//        applyForce(sender: nil)
    }
    
    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        
        applyForce()
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        scnView.backgroundColor = UIColor.blue
        scale = SCNVector3Make(70, 7, 70)
        
        // Register for UITapGestures
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        scnView.addGestureRecognizer(gestureRecognizer)
        
        //spinCoinBtn()
        addObject(sender: nil)          // Add initial scene coin
    }
    
}
