//
//  DiceViewController.swift
//  LuckyTosser
//
//  Created by Steve on 31/03/16.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import AVFoundation
import CoreMotion
import QuartzCore
import SceneKit
import UIKit

class DiceViewController: LuckyTosserViewController, LuckyTosserProtocol {

    var die: Die!
    var diceArray: [SCNNode] = []
    var debugDiceTotalLabel: UILabel?
    
    // MARK: Designated initialiser
    
    init(
        nibName: String?,
        bundle: Bundle,
        boxImageFile: String,
        diceScene: String,
        childNodeName: String,
        rollTossSoundFile: String) {
        
            super.init(
                nibName: nibName,
                bundle: bundle,
                boxImageFile: boxImageFile,
                scene: diceScene,
                childNodeName: childNodeName,
                rollTossSoundFile: rollTossSoundFile)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    // MARK: LuckyTosserProtocol
    
    func addObject(sender: UIButton?) {
        
        //print("Inside func addObject(sender:) of DiceViewController")
        
        //debugDiceTotalLabel?.text = "\(diceArray.count)"
        die = Die(num: _i, importedCube: importedNode, scale: scale)
        diceArray.append(die)
        theScene.rootNode.addChildNode(diceArray[_i])
        playRollTossSoundFX()
        _i += 1
    }
    
    func applyForce() {
        
        if let accelerometerData = motionManager.accelerometerData {

            theScene.physicsWorld.gravity = SCNVector3Make(Float(accelerometerData.acceleration.x * 10000),
                                                           Float(accelerometerData.acceleration.y * -10000),
                                                           Float(accelerometerData.acceleration.z * 5000))
            
            // Make objects stick to table
            let accelX = (abs(accelerometerData.acceleration.x))
            if (accelX < 0.5){
                
                theScene.physicsWorld.gravity = SCNVector3Make(0, -5000, 0)
                
            }
            
            // Apply impulse force in Y to dice only
            if accelerometerData.acceleration.z > 0.3 {
                
                playRollTossSoundFX()
                
                for dice in diceArray {
                    dice.physicsBody?.applyForce(SCNVector3Make(randomFloat(min: 20, max: 50),
                                                                randomFloat(min: 50, max: 100),
                                                                randomFloat(min: 20, max: 50)),
                                                 
                                                                at: SCNVector3Make(randomFloat(min: 0, max: 50),
                                                                                   randomFloat(min: 0, max: 50),
                                                                                   randomFloat(min: 0, max: 50)),
                                                                asImpulse: true)

                }
            }
        }
    }
    
    func deleteObject(sender:UIButton) {
        
        // debugDiceTotalLabel?.text = "\(diceArray.count)"

        if diceArray.count > 1 {
            
            diceArray.last!.removeFromParentNode()
            diceArray.removeLast()
            _i -= 1
        }
    }
    
    //    // CoinViewController does not have a killObj method. This method is not used.
    //    func killObj() {
    //
    //        for dice in diceArray {
    //            print(dice.presentationNode.position.y)
    //            if dice.presentationNode.position.y < 30 {
    //                dice.removeFromParentNode()
    //                print("Killed die")
    //            }
    //        }
    //    }
    
    // OK
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval) {
        update()
    }
    
    // OK
    func update() {
        applyForce()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        scnView.backgroundColor = UIColor.green
        scale = SCNVector3Make(75, 75, 75)
        
        debugDiceTotalLabel = UILabel(frame: CGRect(x: 10, y: 10, width: 100, height: 50))
        debugDiceTotalLabel?.textColor = UIColor.white
        
        scnView.addSubview(debugDiceTotalLabel!)
        
        for _ in 1...2 {
            addObject(sender: nil)      // Add initial scene die (2)
        }
    }

}
