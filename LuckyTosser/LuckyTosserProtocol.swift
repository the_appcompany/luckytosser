//
//  LuckyTosserProtocol.swift
//  LuckyTosser
//
//  Created by Steve on 4/04/16.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import SceneKit
import UIKit

// "@objc" required in order to specify protocol's methods in calls to
// UIControl's addTarget(_:action:for:) method.

@objc protocol LuckyTosserProtocol {

    /* This protocol includes methods which are NOT implemented in, and thus not inherited from,
     * LuckyTosserViewController. Some methods are included here where we want to FORCE subclasses of
     * LuckyTosserViewController to implement them.
     */
    func addObject(sender: UIButton?)
    func applyForce()
    func deleteObject(sender:UIButton)
    func renderer(_ renderer: SCNSceneRenderer, updateAtTime time: TimeInterval)
    func update()
    
    // Even though LuckyTosserViewController has a viewDidLoad method we want to force
    // its subclasses to implement this method.
    func viewDidLoad()
}
