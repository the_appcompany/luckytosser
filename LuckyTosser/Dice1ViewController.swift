//
//  Dice1ViewController.swift
//  LuckyTosser
//
//  Created by Steve on 3/05/16.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import AVFoundation
import CoreMotion
import QuartzCore
import SceneKit
import UIKit

class Dice1ViewController: DiceViewController, TextBubbleProtocol {
    
    var audioPlayer2: AVAudioPlayer!
    var soundPop: NSURL!
    
    var textBubble01: UIImage!
    var textBubble02: UIImage! 
    var textBubble03: UIImage! 
    
    var textBubble01View: UIImageView! 
    var textBubble02View: UIImageView! 
    var textBubble03View: UIImageView! 
    
    func addTextBubbles() {
        
        func animateTextBubble01() {
            textBubble01 = UIImage(named: "art.scnassets/textBubbles01.png") as UIImage?
            textBubble01View = UIImageView(image: textBubble01!)
            textBubble01View.frame = CGRect(x: (posX/2)-150, y: (posY/1.5), width: 250, height: 0)
            textBubble01View.alpha = 1
            view.addSubview(textBubble01View!)
            
            _ = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("playPopSoundFX"), userInfo: nil, repeats: false)
            
            UIView.animateWithDuration(0.5, delay: 2, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                self.textBubble01View.frame = CGRect(x: (self.posX/2)-150, y: (self.posY/1.5), width: 250, height: 135)
                }, completion: nil)
            
            animateTextBubbleOff01()
            
        }
        
        func animateTextBubbleOff01() {
            if (textBubble01View != nil) {
                UIView.animateWithDuration(0.2, delay: 10, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.textBubble01View.frame = CGRect(x: (self.posX/2)-150, y: (self.posY/1.5), width: 250, height: 0)
                    }, completion: { _ in self.textBubble01View.hidden = true; self.textBubble01View.removeFromSuperview(); self.textBubble01View = nil; animateTextBubble02()})
            }
            
        }
        
        func animateTextBubble02() {
            textBubble02 = UIImage(named: "art.scnassets/textBubbles02.png") as UIImage?
            textBubble02View = UIImageView(image: textBubble02!)
            textBubble02View.frame = CGRect(x: (posX/2)-100, y: (posY/4), width: 250, height: 0)
            textBubble02View.alpha = 1
            view.addSubview(textBubble02View!)
            
            _ = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("playPopSoundFX"), userInfo: nil, repeats: false)
            
            UIView.animateWithDuration(0.5, delay: 2, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                self.textBubble02View.frame = CGRect(x: (self.posX/2)-100, y: (self.posY/4), width: 250, height: 135)
                }, completion: nil)
            
            animateTextBubbleOff02()
            
        }
        
        func animateTextBubbleOff02() {
            if (textBubble02View != nil) {
                UIView.animateWithDuration(0.2, delay: 10, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.textBubble02View.frame = CGRect(x: (self.posX/2)-100, y: (self.posY/4), width: 250, height: 0)
                    }, completion: { _ in self.textBubble02View.hidden = true; self.textBubble02View.removeFromSuperview(); self.textBubble02View = nil; animateTextBubble03()})
            }
            
        }
        
        func animateTextBubble03() {
            textBubble03 = UIImage(named: "art.scnassets/textBubbles03.png") as UIImage?
            textBubble03View = UIImageView(image: textBubble03!)
            textBubble03View.frame = CGRect(x: (posX/2)-100, y: (posY/1.7), width: 250, height: 0)
            textBubble03View.alpha = 1
            view.addSubview(textBubble03View!)
            
            _ = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("playPopSoundFX"), userInfo: nil, repeats: false)
            
            UIView.animateWithDuration(0.5, delay: 2, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                self.textBubble03View.frame = CGRect(x: (self.posX/2)-100, y: (self.posY/1.7), width: 250, height: 135)
                }, completion: nil)
            
            animateTextBubbleOff03()
            
        }
        
        func animateTextBubbleOff03() {
            if (textBubble03View != nil) {
                UIView.animateWithDuration(0.2, delay: 10, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                    self.textBubble03View.frame = CGRect(x: (self.posX/2)-100, y: (self.posY/1.7), width: 250, height: 0)
                    }, completion: { _ in self.textBubble03View.hidden = true; self.textBubble03View.removeFromSuperview(); self.textBubble03View = nil})
            }
            
        }
        animateTextBubble01()
    }
    
    // OK
    func playPopSoundFX() {
        
        // Play sound
        do {
            audioPlayer2 = try AVAudioPlayer(contentsOfURL: soundPop!)
            audioPlayer2.play()
        }catch{
            print("No sound found by URL:\(soundPop)")
        }
    }
    
    
    override func loadSoundFX() {
        
        soundURL1 = NSBundle.mainBundle().URLForResource(loadSound, withExtension: "mp3")
        soundPop = NSBundle.mainBundle().URLForResource("art.scnassets/pop-sound", withExtension: "wav")
    }
    
}
