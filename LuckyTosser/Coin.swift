//
//  Coin.swift
//  LuckyTosser
//
//  Created by JeremyRaven on 11/02/2016.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import SceneKit


class Coin: SCNNode {
    
    var _image: UIImage!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Designated Initialiser
    init(importedCoin: SCNNode, scale: SCNVector3) {
        
        let coin: SCNNode = importedCoin.copy() as! SCNNode
        
        super.init()
        
            coin.scale = scale
        
            self.addChildNode(coin)
            let collisionBox = SCNCylinder(radius: 65, height: 10)
            self.physicsBody?.physicsShape = SCNPhysicsShape(geometry: collisionBox, options: nil)
            self.physicsBody = SCNPhysicsBody.dynamic()
            self.physicsBody?.mass = 0.8
            self.physicsBody?.restitution = 0.9
            self.physicsBody?.damping = 0.5
            self.name = "coin"
            self.physicsBody?.allowsResting = true
            self.position = SCNVector3Make(0, 70, -50)
            self.eulerAngles = SCNVector3Make( 0, GLKMathDegreesToRadians(90), 0)

    }
    
    func randomFloat(min: Float, max:Float) -> Float {
        return min + Float(arc4random_uniform(UInt32(max - min + 1)))
    }
}
