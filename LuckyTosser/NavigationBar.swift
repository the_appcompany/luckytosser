//
//  NavigationBar.swift
//  LuckyTosser
//
//  Created by JeremyRaven on 13/02/2016.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import UIKit

class NavigationBar: UIView {

    // OK (variable declarations)
    var sliderViewUI: UIView!
    //var sideBar: UIView
    //var barView: UIView
    var posY: CGFloat = 0.0
    var posX: CGFloat = 0.0
    var dice1Btn: UIButton!
    var dice2Btn: UIButton!
    var coin1Btn: UIButton!
    var coin2Btn: UIButton!
    var dice3Btn: UIButton!
    var dice4Btn: UIButton!
    var navPos: CGPoint!

    // OK
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // OK
    init(frame: CGRect, pos: CGPoint) {
        
        posY = frame.height
        posX = frame.width
        navPos = pos
        
        super.init(frame: frame)
        
        self.addSliderViewUI()
        self.addButtons()
    }
    
    // OK
    func addSliderViewUI() {
        
        sliderViewUI = UIView(frame: CGRectMake(navPos.x, navPos.y, 580, 80))
        let sideBar1 = sideBarView(0.0, y:0.0, w: 50.0, h: 80.0)
        let sideBar2 = sideBarView(530, y:0.0, w: 50.0, h: 80.0)
        dice1Btn = dice1ViewControllerBtn()
        dice2Btn = dice2ViewControllerBtn()
        coin1Btn = coin1ViewControllerBtn()
        coin2Btn = coin2ViewControllerBtn()
        dice3Btn = dice3ViewControllerBtn()
        dice4Btn = dice4ViewControllerBtn()

        sliderViewUI.addSubview(sideBar1)
        sliderViewUI.addSubview(sideBar2)
        sliderViewUI.addSubview(dice1Btn)
        sliderViewUI.addSubview(dice2Btn)
        sliderViewUI.addSubview(coin1Btn)
        sliderViewUI.addSubview(coin2Btn)
        sliderViewUI.addSubview(dice3Btn)
        sliderViewUI.addSubview(dice4Btn)

        self.addSubview(sliderViewUI)
    }
    
    // OK
    func sideBarView(x: CGFloat, y: CGFloat, w: CGFloat, h: CGFloat) -> UIView{
        let barView = UIView(frame: CGRectMake(x, y, w, h))
        barView.backgroundColor = (UIColor.lightGrayColor())
        return barView
    }
    
    // OK
    func dice1ViewControllerBtn() -> UIButton{
        let diceImage = UIImage(named: "art.scnassets/dice1Image.png") as UIImage?
        let dice1Btn = UIButton(type: UIButtonType.Custom) as UIButton
        dice1Btn.frame = CGRectMake(50, 0, 80, 80)
        dice1Btn.setImage(diceImage, forState: .Normal)
        dice1Btn.addTarget(nil, action:"segueToDice1", forControlEvents:.TouchUpInside)
        return dice1Btn
    }
    
    // OK
    func dice2ViewControllerBtn() -> UIButton{
        let dice2Image = UIImage(named: "art.scnassets/dice2ImageBW.png") as UIImage?
        let dice2Btn = UIButton(type: UIButtonType.Custom) as UIButton
        dice2Btn.frame = CGRectMake(210, 0, 80, 80)
        dice2Btn.setImage(dice2Image, forState: .Normal)
        dice2Btn.addTarget(nil, action:("segueToDice2"), forControlEvents:.TouchUpInside)
        return dice2Btn
    }

    // OK
    func dice3ViewControllerBtn() -> UIButton{
        let dice3Image = UIImage(named: "art.scnassets/dice3ImageBW.png") as UIImage?
        let dice3Btn = UIButton(type: UIButtonType.Custom) as UIButton
        dice3Btn.frame = CGRectMake(370, 0, 80, 80)
        dice3Btn.setImage(dice3Image, forState: .Normal)
        dice3Btn.addTarget(nil, action:("segueToDice3"), forControlEvents:.TouchUpInside)
        return dice3Btn
    }
    
    // OK
    func dice4ViewControllerBtn() -> UIButton{
        let dice4Image = UIImage(named: "art.scnassets/dice4ImageBW.png") as UIImage?
        let dice4Btn = UIButton(type: UIButtonType.Custom) as UIButton
        dice4Btn.frame = CGRectMake(450, 0, 80, 80)
        dice4Btn.setImage(dice4Image, forState: .Normal)
        dice4Btn.addTarget(nil, action:("segueToDice4"), forControlEvents:.TouchUpInside)
        return dice4Btn
    }
    
    // OK
    func coin1ViewControllerBtn() -> UIButton{
        let coin1Image = UIImage(named: "art.scnassets/coin1ImageBW.png") as UIImage?
        let coin1Btn = UIButton(type: UIButtonType.Custom) as UIButton
        coin1Btn.frame = CGRectMake(130, 0, 80, 80)
        coin1Btn.setImage(coin1Image, forState: .Normal)
        coin1Btn.addTarget(nil, action:("segueToCoin1"), forControlEvents:.TouchUpInside)
        return coin1Btn
    }
    
    // OK
    func coin2ViewControllerBtn() -> UIButton{
        let coin2Image = UIImage(named: "art.scnassets/coin2ImageBW.png") as UIImage?
        let coin2Btn = UIButton(type: UIButtonType.Custom) as UIButton
        coin2Btn.frame = CGRectMake(290, 0, 80, 80)
        coin2Btn.setImage(coin2Image, forState: .Normal)
        coin2Btn.addTarget(nil, action:("segueToCoin2"), forControlEvents:.TouchUpInside)
        return coin2Btn
    }
    
    // OK
    func addButtons() {
        let image1 = UIImage(named: "art.scnassets/addBtn.png") as UIImage?
        let addBtn = UIButton(type: UIButtonType.Custom) as UIButton
        addBtn.frame = CGRectMake(-4, 0, 50, 80)
        addBtn.setImage(image1, forState: .Normal)
        addBtn.addTarget(nil, action:("addObject:"), forControlEvents:.TouchUpInside)
        self.addSubview(addBtn)
        
        let image2 = UIImage(named: "art.scnassets/deleteBtn.png") as UIImage?
        let deleteBtn = UIButton(type: UIButtonType.Custom) as UIButton
        deleteBtn.frame = CGRectMake(posX-46, 0, 50, 80)
        deleteBtn.setImage(image2, forState: .Normal)
        deleteBtn.addTarget(nil, action:("deleteObject:"), forControlEvents:.TouchUpInside)
        self.addSubview(deleteBtn)
    }
    
}