//
//  Coin1ViewController.swift
//  LuckyTosser
//
//  Created by Steve on 25/02/16.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
import CoreMotion
import AVFoundation

//class Coin1ViewController: LuckyTosserViewController, SCNSceneRendererDelegate {
class Coin1ViewController {

    let Gravity: Float = 0
    let impulseForce: CGFloat = 1.5
    var _size: CGSize!
    var posX: CGFloat = 0.0
    var posY: CGFloat = 0.0
    var rect: CGRect!
    var motionManager: CMMotionManager!
    var coinObject: SCNNode?
    var audioPlayer1: AVAudioPlayer!
    var soundURL1: NSURL!
    var scale: SCNVector3!
    var importedSceneCoinNZ: SCNScene!
    var importedCoinNZ: SCNNode!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame = UIScreen.mainScreen().bounds;
        scnView = self.view as! SCNView                                     // Downcast view to a SCNView
        posX = scnView.bounds.width
        posY = scnView.bounds.height
        rect = CGRectMake(0, posY, posX, 100)
        // scnView.allowsCameraControl = true
        // scnView.showsStatistics = true
        // _size = scnView.bounds.size
        
        theScene = SCNScene()                                               // Create instance of SCNScene
        theScene.physicsWorld.gravity = SCNVector3(0, Gravity, 0)
        
        scnView.scene = theScene                                            // Add SCNScene to current view
        // MARK: #1
        scnView.backgroundColor = UIColor.blueColor()
        // MARK: #2
        _image = UIImage(named: "art.scnassets/coin1_1420x2205.png") as UIImage?
        // MARK: #3
        scale = SCNVector3Make(60, 4, 60)
        scnView.delegate = self
        
        panRec.addTarget(self, action: "dragSliderView:")                   // Setup pan gesture
        let navBarPos = CGPointMake(0, 0)

        motionManager = CMMotionManager()                                   // Start the Accelerometer
        motionManager.startAccelerometerUpdates()
        
        // Call functions
        self.createNavBar(rect, pos: navBarPos)
        self.loadMeshObjects()
        self.loadSoundFX()                                                  // Load SoundFX
        self.makeConstraintObject()                                         // Make constraint
        self.addLightSourceNode(constraint)                                 // Add light
        self.addCameraNode(constraint)                                      // Add Camera
        self.makeBox(_image)                                                // Make collision world
        self.animateNavBarUp()
        // MARK: #3
        self.spinCoinBtn()
        // MARK: #4
        self.addInitCoin()                                              // Add initial scene coin
        
        
    }
    
    func spinCoinBtn() {
        let spinCoinBtn = UIButton(type: UIButtonType.Custom) as UIButton
        spinCoinBtn.frame = CGRectMake(posX / 2 - 40, posY / 3 + 250, 100, 100)
        spinCoinBtn.layer.cornerRadius = 50;
        spinCoinBtn.clipsToBounds = true;
        spinCoinBtn.backgroundColor = UIColor.grayColor()
        spinCoinBtn.setTitle("tap tap", forState: UIControlState.Normal)
        spinCoinBtn.addTarget(nil, action:("applyForceCoin:"), forControlEvents:.TouchUpInside)
        
        scnView.addSubview(spinCoinBtn)
    }
    
    func addObject(sender:UIButton) {
        //print(__FUNCTION__)
        coinObject!.removeFromParentNode()
        coinObject = Coin(importedCoin: importedCoinNZ)
        theScene.rootNode.addChildNode(coinObject!)
        
    }
    
    func deleteObject(sender:UIButton) {
        //print(__FUNCTION__)
        if coinObject != nil {
            coinObject!.removeFromParentNode()
        }else {
            print("Object is nil")
        }
    }
    
    // Doesnt require selector for UIButton
    func addInitCoin() {
        coinObject = Coin(importedCoin: importedCoinNZ)
        
        if coinObject != nil{
            theScene.rootNode.addChildNode(coinObject!)
            print(coinObject!)

        }else{
            print("It is nil")
        }
    }
    
    func loadSoundFX() {
        
        audioPlayer1 = AVAudioPlayer()
        soundURL1 = NSBundle.mainBundle().URLForResource("art.scnassets/coinToss", withExtension: "mp3")
    }
    
    func loadMeshObjects() {
        
        importedSceneCoinNZ = SCNScene(named: "art.scnassets/coin.dae")
        importedCoinNZ = importedSceneCoinNZ.rootNode.childNodeWithName("coinNZ", recursively: true)

    }
    
    func applyForceCoin(sender: UIButton)
        {
        if let accelerometerData = motionManager.accelerometerData
            {
            playCoinSoundFX()
            coinObject!.physicsBody?.applyForce(SCNVector3Make(0, randomFloat(800, max: 1000), 0), atPosition: SCNVector3Make(0, 0, 100), impulse: true)
            }
            if let accelerometerData = motionManager.accelerometerData                              // Make objects stick to table
                {
                let accelX = (abs(accelerometerData.acceleration.x))
                    if (accelX < 0.3)
                    {
                    theScene.physicsWorld.gravity = SCNVector3Make(0, -5000, 0)
                    }
                }
        }
        
    func playCoinSoundFX() { // Not implemented
        // Play sound
        do {
            audioPlayer1 = try AVAudioPlayer(contentsOfURL: soundURL1!)
        }catch{
            print("No sound found by URL:\(soundURL1)")
        }
        audioPlayer1.play()
    }
    
    func dragSliderView(gesture:UIPanGestureRecognizer){
        
        let translation: CGPoint
        self.view.bringSubviewToFront(gesture.view!)
        translation = gesture.translationInView(self.view)
        
        switch (gesture.state){
        case UIGestureRecognizerState.Began:
            
            break
            
        case UIGestureRecognizerState.Changed:
            
            gesture.view!.center = CGPointMake(gesture.view!.center.x + translation.x, gesture.view!.center.y)
            print("SliderView center is \(gesture.view!.center.x)")
            
        case UIGestureRecognizerState.Ended:
            
            
            var finalX: CGFloat = translation.x + (2.0 * (gesture.velocityInView(self.view).x))
            print("FinalX is \(finalX)")
            //var finalY: CGFloat = translation.y + (0.0 * (gesture.velocityInView(self.view).y))
            
            if finalX > 289 {
                finalX = 289
            }
            if finalX < 88 {
                finalX = 88
            }
            
            UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options:  .CurveEaseIn, animations: {gesture.view!.center.x = finalX}, completion: nil)
            
        default:
            break
        }
        
        gesture.setTranslation(CGPointZero, inView: self.view)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let _ = touches.first {
            
            if self.navBar!.center.y > posY {
                
                UIView.animateWithDuration(0.5, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options:  .CurveEaseOut, animations: {
                    self.navBar!.center.y -= 100}, completion: nil)
            }else{
                
                UIView.animateWithDuration(0.5, delay: 0.5, options: .CurveEaseOut, animations: {
                    self.navBar!.center.y += 100}, completion: nil)
            }
        }
    }
    
    func update() {
        //applyForceCoin()
    }
    
    func renderer(aRenderer: SCNSceneRenderer, updateAtTime time: NSTimeInterval) {
        
        self.update()
    }

}