//
//  Dice2ViewController.swift
//  LuckyTosser
//
//  Created by Steve on 25/02/16.
//  Copyright © 2016 TheAppCompany. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
import CoreMotion
import AVFoundation

class Dice2ViewController: LuckyTosserViewController, SCNSceneRendererDelegate {
    
    var die: Die!
    var _i: Int = 0
    var _j: Int = 0
    let Gravity: Float = 0
    let impulseForce: CGFloat = 1.5
    var _size: CGSize!
    var importedSceneDiceChina: SCNScene!
    var importedCube2: SCNNode!
    var posX: CGFloat = 0.0
    var posY: CGFloat = 0.0
    var rect: CGRect!
    var motionManager: CMMotionManager!
    var diceArray: [SCNNode] = []
    var audioPlayer1: AVAudioPlayer!
    var soundURL1: NSURL!
    var scale: SCNVector3!

    
    //let dynamicAnimator = UIDynamicAnimator()
    //let attachmentBehaviour = UIAttachmentBehavior()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame = UIScreen.mainScreen().bounds;
        scnView = self.view as! SCNView                                     // Downcast view to a SCNView
        posX = scnView.bounds.width
        posY = scnView.bounds.height
        rect = CGRectMake(0, posY, posX, 100)
        // scnView.allowsCameraControl = true
        // scnView.showsStatistics = true
        // _size = scnView.bounds.size
        
        theScene = SCNScene()                                               // Create instance of SCNScene
        theScene.physicsWorld.gravity = SCNVector3(0, Gravity, 0)
        
        scnView.scene = theScene                                            // Add SCNScene to current view
        scnView.backgroundColor = UIColor.redColor()
        _image = UIImage(named: "art.scnassets/monkey1242x2208.png") as UIImage?
        scale = SCNVector3Make(75, 75, 75)
        scnView.delegate = self
        
        panRec.addTarget(self, action: "dragSliderView:")                   // Setup pan gesture
        let navBarPos = CGPointMake(-65, 0)

        motionManager = CMMotionManager()                                   // Start the Accelerometer
        motionManager.startAccelerometerUpdates()
        
        // Call functions
        self.createNavBar(rect, pos: navBarPos)
        self.loadMeshObjects()                                              // Load mesh objects
        self.loadSoundFX()                                                  // Load SoundFX
        self.makeConstraintObject()                                         // Make constraint
        self.addLightSourceNode(constraint)                                 // Add light
        self.addCameraNode(constraint)                                      // Add Camera
        self.makeBox(_image)                                    // Make collision world
        self.animateNavBarUp()
        
        for var i = 1; i <= 2; ++i {
            self.addInitDice()                                              // Add initial scene die (2)
        }
        
    }
    
    func addObject(sender:UIButton) {
        die = Die(num: _i, importedCube: importedCube2, scale: scale)
        diceArray.append(die)
        theScene.rootNode.addChildNode(diceArray[_i])
        playRollDiceSoundFX()
        _i += 1
    }
    
    func deleteObject(sender:UIButton) {
        if diceArray.count > 1 {
            diceArray.last!.removeFromParentNode()
            diceArray.removeLast()
            _i -= 1
        }
    }
    
    // Doesnt require selector for UIButton
    func addInitDice() {
        die = Die(num: _i, importedCube: importedCube2, scale: scale)
        diceArray.append(die)
        theScene.rootNode.addChildNode(diceArray[_i])
        playRollDiceSoundFX()
        _i += 1
    }
    
    func loadSoundFX() {
        
        audioPlayer1 = AVAudioPlayer()
        soundURL1 = NSBundle.mainBundle().URLForResource("art.scnassets/RollDice", withExtension: "mp3")
    }
    
    func loadMeshObjects() {
        
        importedSceneDiceChina = SCNScene(named: "art.scnassets/diceChinese.dae")
        importedCube2 = importedSceneDiceChina.rootNode.childNodeWithName("ChineseDie", recursively: true)
    }
    
    func applyForceDice() {
        
        // let _force: Float = 10000.0
        if let accelerometerData = motionManager.accelerometerData {
            theScene.physicsWorld.gravity = SCNVector3Make(Float(accelerometerData.acceleration.x * 10000), Float(accelerometerData.acceleration.z * 5000), Float(accelerometerData.acceleration.y * -10000))
            
            // Make objects stick to table
            let accelX = (abs(accelerometerData.acceleration.x))
            if (accelX < 0.3){
                theScene.physicsWorld.gravity = SCNVector3Make(0, -5000, 0)
                
            }

            // Apply impulse force in Y to dice only
            if accelerometerData.acceleration.z > 0.3 {
                playRollDiceSoundFX()
                //                if currentUIView.navBar.center.y < currentUIView.posY {
                //                    print(currentUIView.navBar.center.y)
                //                    currentUIView.animateNavBarDown()
                //                }
                for dice in diceArray {
                    dice.physicsBody?.applyForce(SCNVector3Make(randomFloat(20, max: 50), randomFloat(50, max: 100), randomFloat(20, max: 50)), atPosition: SCNVector3Make(randomFloat(0, max: 50), randomFloat(0, max: 50), randomFloat(0, max: 50)), impulse: true)
                }
            }
        }
    }
    
    func killObj() {
        
        for dice in diceArray {
            print(dice.presentationNode.position.y)
            if dice.presentationNode.position.y < 30 {
                dice.removeFromParentNode()
                print("Killed die")
            }
        }
        
    }
    
    func playRollDiceSoundFX() { // Not implemented
        // Play sound
        do {
            audioPlayer1 = try AVAudioPlayer(contentsOfURL: soundURL1!)
        }catch{
            print("No sound found by URL:\(soundURL1)")
        }
        audioPlayer1.play()
    }

    func dragSliderView(gesture:UIPanGestureRecognizer){
        
        let translation: CGPoint
        self.view.bringSubviewToFront(gesture.view!)
        translation = gesture.translationInView(self.view)
        
        switch (gesture.state){
        case UIGestureRecognizerState.Began:
            
            break
            
        case UIGestureRecognizerState.Changed:
            
            gesture.view!.center = CGPointMake(gesture.view!.center.x + translation.x, gesture.view!.center.y)
            print("SliderView center is \(gesture.view!.center.x)")
            
        case UIGestureRecognizerState.Ended:
            
            
            var finalX: CGFloat = translation.x + (2.0 * (gesture.velocityInView(self.view).x))
            print("FinalX is \(finalX)")
            //var finalY: CGFloat = translation.y + (0.0 * (gesture.velocityInView(self.view).y))
            
            if finalX > 289 {
                finalX = 289
            }
            if finalX < 88 {
                finalX = 88
            }
            
            UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options:  .CurveEaseIn, animations: {gesture.view!.center.x = finalX}, completion: nil)
            
        default:
            break
        }
        
        gesture.setTranslation(CGPointZero, inView: self.view)
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let _ = touches.first {
            
            if self.navBar!.center.y > posY {
                
                UIView.animateWithDuration(0.5, delay: 0.5, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options:  .CurveEaseOut, animations: {
                    self.navBar!.center.y -= 100}, completion: nil)
            }else{
                
                UIView.animateWithDuration(0.5, delay: 0.5, options: .CurveEaseOut, animations: {
                    self.navBar!.center.y += 100}, completion: nil)
            }
        }
    }
    
    func update() {
        applyForceDice()
        //killObj()
    }
    
    func renderer(aRenderer: SCNSceneRenderer, updateAtTime time: NSTimeInterval) {
        
        self.update()
    }
        
}