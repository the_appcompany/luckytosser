//
//  RedDragonCoin.swift
//  LuckyTosser
//
//  Created by Steve on 10/07/18.
//  Copyright © 2018 TheAppCompany. All rights reserved.
//

import Foundation

struct RedDragonCoin {
    static let nibName              = "Coin1ViewController"
    static let bundle               = Bundle.main
    static let boxImageFile         = "art.scnassets/DiceWallpaper.png"
    static let coinScene            = "art.scnassets/RedDragonCoin.dae"
    static let childNodeName        = "coinNZ"
    static let rollTossSoundFile    = "art.scnassets/CoinToss"
}
